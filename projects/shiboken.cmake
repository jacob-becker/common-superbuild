set(shiboken_extra_args)
if (UNIX)
  list(APPEND shiboken_extra_args
    -DPYTHON_SITE_PACKAGES:STRING=lib/python2.7/site-packages)
endif ()

superbuild_add_project(shiboken
  DEPENDS qt4 python
  CMAKE_ARGS
    -DSET_RPATH:BOOL=ON
    -DBUILD_SHARED_LIBS:BOOL=ON
    -DDISABLE_DOCSTRINGS:BOOL=ON
    -DBUILD_TESTS:BOOL=OFF
    ${shiboken_extra_args})
