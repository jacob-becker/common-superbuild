set(CPACK_PACKAGE_DESCRIPTION_SUMMARY
  "CMB ModelBuilder Application")
set(CPACK_PACKAGE_NAME "CMB")
set(cmb_package_name "ModelBuilder")

set(cmb_programs_to_install
  ModelBuilder)
include(cmb.bundle.common)

set(cmb_plugins
  smtkDiscreteSessionPlugin
  smtkExodusSessionPlugin
  smtkPolygonSessionPlugin
  smtkRemoteSessionPlugin
  smtkRemusMeshOperatorPlugin
  CMB_Plugin
  ModelBridge_Plugin)

if (cgm_enabled)
  list(APPEND cmb_plugins
    smtkCGMSessionPlugin)
endif ()

set(cmb_plugins_file "${CMAKE_CURRENT_BINARY_DIR}/.plugins")
cmb_add_plugin(
  "${cmb_plugins_base_file}"
  "${cmb_plugins_file}"
  ${cmb_plugins})

set(cmb_plugins_to_install
  ${cmb_plugins})

set(cmb_install_paraview_server FALSE)
